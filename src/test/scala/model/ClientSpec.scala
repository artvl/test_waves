package model

import org.scalatest.{DiagrammedAssertions, FunSuite, Matchers, WordSpecLike}

class ClientSpec extends FunSuite with DiagrammedAssertions
  with Matchers {

  test("throw assert on withdraw when insufficient usd") {
    val client = Client("", 5, Map.empty)
    client.enoughUsd(10) shouldBe false
    a[AssertionError] should be thrownBy client.withdrawUsd(10)
  }

  test("throw assert on withdraw when insufficient paper") {
    val client = Client("", 5, Map.empty)
    client.enoughPaper("A", 5) shouldBe false
    a[AssertionError] should be thrownBy client.withdrawPaper("A", 10)
  }

  test("withdraw paper should return new client without paper") {
    val client = Client("", 5, Map("A" -> 5))
    client.enoughPaper("A", 3)
    val r = client.withdrawPaper("A", 3)
    r.map("A") shouldBe 2
  }

  test("add paper should return new client with paper") {
    val client = Client("", 5, Map("A" -> 5))
    val r = client.addPaper("B", 3)
    r.map.size shouldBe 2
    r.map("B") shouldBe 3
  }

}
