package model

import org.scalatest.{FunSuite, Matchers}

class PaperMarketSpec extends FunSuite
  with Matchers {

  test("place sell order should add sell order in tree, if no buy") {
    val pm = PaperMarket.empty()

    val order = Order("C", SELL, "A", 10, 10, 0)
    val res = pm.placeOrder(order)

    res.market.sell.size shouldBe 1
  }

  test("place buy order should add buy order in tree, if no sell") {
    val pm = PaperMarket.empty()

    val order = Order("C", BUY, "A", 10, 10, 0)
    val res = pm.placeOrder(order)

    res.market.buy.size shouldBe 1
  }

  test("place buy order should add buy order in tree," +
    "if no matching prices") {
    val sell = Order("C", SELL, "A", 20, 10, 0)
    val pm = PaperMarket.empty().placeOrder(sell).market

    val order = Order("C2", BUY, "A", 10, 10, 0)

    val res = pm.placeOrder(order)
    res.orderResult.ops.size shouldBe 0
    res.market.buy.size shouldBe 1
    res.market.sell.size shouldBe 1
  }

  test("place sell order should add partially completed " +
    "sell order in tree") {
    val sell = Order("C", BUY, "A", 10, 10, 0)
    val pm = PaperMarket.empty().placeOrder(sell).market

    val order = Order("C2", SELL, "A", 10, 20, 0)

    val res = pm.placeOrder(order)
    res.orderResult.ops.size shouldBe 1
    res.market.buy.size shouldBe 0
    res.market.sell.size shouldBe 1
    res.market.sell.head.amt shouldBe 10
  }

  test("place small buy order should not add any," +
    " if it can be completed immediately") {
    val sell = Order("C", SELL, "A", 20, 20, 0)
    val pm = PaperMarket.empty().placeOrder(sell).market

    val order = Order("C2", BUY, "A", 50, 10, 0)

    val res = pm.placeOrder(order)
    res.orderResult.ops.size shouldBe 1
    val op = res.orderResult.ops.head
    op.amt shouldBe 10
    op.price shouldBe 20
    op.from shouldBe "C"
    op.to shouldBe "C2"
    res.market.buy.size shouldBe 0
    res.market.sell.size shouldBe 1
    res.market.sell.head.amt shouldBe 10
  }

  test("two equal opposite orders should be completed") {
    val sell = Order("C", SELL, "A", 20, 20, 0)
    val buy = Order("C2", BUY, "A", 20, 20, 0)
    val pm = PaperMarket.empty().placeOrder(sell).market
    val res = pm.placeOrder(buy)
    res.market.sell.size shouldBe 0
    res.market.buy.size shouldBe 0
    res.orderResult.ops.size shouldBe 1
    res.orderResult.ops.head.amt shouldBe 20
  }

  test("place two sell ord with same price according to time," +
    "and complete it corresponding to order") {
    val pm = PaperMarket.empty()
    val s1 = Order("C", SELL, "A", 20, 20, 0)
    val s2 = Order("C1", SELL, "A", 20, 20, 1)
    val res = pm.placeOrder(s1).market.placeOrder(s2)
    res.market.sell.head.clientId shouldBe "C"
    res.market.sell.last.clientId shouldBe "C1"

    val buy = Order("C2", BUY, "A", 30, 30, 2)
    val buyRes = res.market.placeOrder(buy)

    val orderRes = buyRes.orderResult
    val market = buyRes.market
    market.sell.size shouldBe 1
    market.sell.head.clientId shouldBe "C1"

    orderRes.ops.size shouldBe 2
    orderRes.ops.head.amt shouldBe 10
  }

  test("place two sell ord with different price" +
    "and complete it corresponding to order") {
    val pm = PaperMarket.empty()
    val s1 = Order("C", SELL, "A", 20, 20, 0)
    val s2 = Order("C1", SELL, "A", 30, 20, 1)
    val res = pm.placeOrder(s1).market.placeOrder(s2)
    res.market.sell.head.clientId shouldBe "C"
    res.market.sell.last.clientId shouldBe "C1"

    val buy = Order("C2", BUY, "A", 40, 30, 2)
    val buyRes = res.market.placeOrder(buy)

    val orderRes = buyRes.orderResult
    val market = buyRes.market
    market.sell.size shouldBe 1
    market.sell.head.clientId shouldBe "C1"

    orderRes.ops.size shouldBe 2
    orderRes.ops.head.amt shouldBe 10
    orderRes.ops.head.price shouldBe 30
    orderRes.ops.tail.head.price shouldBe 20
  }


}
