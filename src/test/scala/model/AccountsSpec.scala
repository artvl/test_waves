package model

import org.scalatest.{FunSuite, Matchers}

class AccountsSpec extends FunSuite
  with Matchers {

  val client = Client("C", 5, Map("A" -> 5))

  test("accounts should decline order if client has insufficient usd funds") {
    val acc = Accounts(Map("C" -> client))
    val order = Order("C", BUY, "E", 10, 1, 0)
    val result = acc.updateOrder(order)
    result.amt shouldBe 0
  }

  test("accounts should decline order if client has insufficient paper funds") {
    val acc = Accounts(Map("C" -> client))
    val order = Order("C", SELL, "E", 10, 1, 0)
    val result = acc.updateOrder(order)
    result.amt shouldBe 0
  }

  test("accounts should do not touch order if it is ok") {
    val acc = Accounts(Map("C" -> client))
    val order = Order("C", BUY, "E", 3, 1, 0)
    val result = acc.updateOrder(order)
    result.amt shouldBe 1
  }

  test("book buy order") {
    val acc = Accounts(Map("C" -> client))
    val order = Order("C", BUY, "E", 3, 1, 0)
    val result = acc.bookOrder(order)
    result.clients("C").usd shouldBe 2
  }

  test("book sell order") {
    val acc = Accounts(Map("C" -> client))
    val order = Order("C", SELL, "A", 1, 3, 0)
    val result = acc.bookOrder(order)
    result.clients("C").paperAmount("A") shouldBe 2
  }

  test("unbook buy order") {
    val acc = Accounts(Map("C" -> client))
    val order = Order("C", BUY, "E", 3, 1, 0)
    val result = acc.unbookOrder(order)
    result.clients("C").usd shouldBe 8
  }

  test("unbook sell order") {
    val acc = Accounts(Map("C" -> client))
    val order = Order("C", SELL, "D", 1, 3, 0)
    val result = acc.unbookOrder(order)
    result.clients("C").paperAmount("A") shouldBe 5
    result.clients("C").paperAmount("D") shouldBe 3
  }

  test("apply operation from the same client") {
    val c1 = client.copy(id = "C1", usd = 5, map = Map.empty)
    val acc = Accounts(Map(c1.id -> c1))
    val op = Operation(c1.id, c1.id, "A", 3, 1)
    val res = acc.applyOperation(op)
    val clieRes = res.clients("C1")
    clieRes.usd shouldBe 8
    clieRes.paperAmount("A") shouldBe (3)
  }

  test("apply operation") {
    val c1 = client.copy(id = "C1", usd = 5)
    val c2 = client.copy(id = "C2", usd = 10)
    val acc = Accounts(Map(c1.id -> c1, c2.id -> c2))
    val op = Operation(c1.id, c2.id, "A", 3, 1)
    val res = acc.applyOperation(op)
    val c1Res = res.clients("C1")
    val c2Res = res.clients("C2")
    c1Res.usd shouldBe 8
    c2Res.paperAmount("A") shouldBe 8
  }

}
