package model

import org.scalatest.{DiagrammedAssertions, FunSuite, Matchers}

class OrderSpec extends FunSuite with DiagrammedAssertions
  with Matchers {

  test("order part complete should throw " +
    "assertion error when insufficient amt") {
    val o1 = Order("C", BUY, "A", 3, 3, 0)
    val o2 = Order("C", SELL, "A", 3, 5, 0)
    a[AssertionError] should be thrownBy o1.partiallyComplete(o2, o2.price)
  }

  test("order part complete should throw " +
    "assertion error when used with not opposite") {
    val o1 = Order("C", BUY, "A", 3, 5, 0)
    val o2 = Order("C", BUY, "A", 3, 5, 0)
    a[AssertionError] should be thrownBy o1.partiallyComplete(o2, o2.price)
  }


  test("order part complete should throw " +
    "assertion error when other paper") {
    val o1 = Order("C", BUY, "A", 3, 5, 0)
    val o2 = Order("C", SELL, "B", 3, 5, 0)
    a[AssertionError] should be thrownBy o1.partiallyComplete(o2, o2.price)
  }

  test("part complete should return new order with amt diff") {
    val o1 = Order("C", BUY, "A", 3, 5, 0)
    val o2 = Order("D", SELL, "A", 3, 5, 0)
    val (op, res) = o1.partiallyComplete(o2, o2.price)
    res.amt shouldBe 0
    op.usdCost shouldBe 5 * 3
    op.from shouldBe "D"
    op.to shouldBe "C"

    val (op1, res1) = o2.partiallyComplete(o1, o2.price)
    res1.amt shouldBe 0
    op1.usdCost shouldBe 5 * 3
    op1.from shouldBe "D"
    op1.to shouldBe "C"
  }

}
