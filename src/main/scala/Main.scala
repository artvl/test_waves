
import java.io.FileWriter

import engine.Engine
import model.{Accounts, Client, ExchangeState, Order}

object Main extends App {

  import scala.io.Source

  val clientsFile: Iterator[String] = Source.fromResource("clients.txt").getLines
  val ordersFile: Iterator[String] = Source.fromResource("orders.txt").getLines

  val clients = clientsFile.map(Client.parse).toIterable
  val accountsState = Accounts.create(clients)
  val state = ExchangeState(accountsState, Map.empty)

  val orders = ordersFile.map(Order.parse).toIterable

  val engine = new Engine

  val processAllOrders = engine.process(state, orders)

  val balances = processAllOrders.cancelAllOrders()

  val fw = new FileWriter("./result.txt")
  balances.accountsState.clients.values.toSeq.sortBy(_.id)
    .foreach{
      client =>
        fw.append(client.toString() + "\n")
    }

  fw.close()










}
