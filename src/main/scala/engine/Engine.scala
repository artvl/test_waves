package engine

import model.{Order, ExchangeState}

class Engine {


  def process(state: ExchangeState, orders: Iterable[Order]): ExchangeState = {
    val res = orders.foldLeft(state) {
      (state: ExchangeState, order: Order) =>
        state.placeOrder(order)
    }
    res
  }

}
