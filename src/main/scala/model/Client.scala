package model

case class Client(id: String, usd: Amount, map: Map[Paper, Amount]) {
  def addPaper(p: Paper, amt: Amount): Client = {
    val oldAmt = map.getOrElse(p, 0L)
    val newAmt = oldAmt + amt
    val remove = map - p
    val newMap = remove + (p -> newAmt)
    this.copy(map = newMap)
  }

  def addUsd(amt: Amount): Client = {
    this.copy(usd = usd + amt)
  }

  def enoughPaper(p: Paper, amt: Amount): Boolean = {
    val c = map.getOrElse(p, 0L)
    c >= amt
  }

  def enoughUsd(amt: Amount): Boolean = {
    usd >= amt
  }

  def paperAmount(p: Paper): Amount = {
    map.getOrElse(p, 0L)
  }

  def usdAmount(): Amount = {
    usd
  }

  def withdrawPaper(p: Paper, amt: Amount): Client = {
    val oldAmt = map.getOrElse(p, 0L)
    val newAmt = oldAmt - amt
    assert(newAmt >= 0)
    val remove = map - p
    val newMap = remove + (p -> newAmt)
    this.copy(map = newMap)
  }

  def withdrawUsd(amt: Amount): Client = {
    val newAmt = usd - amt
    assert(newAmt >= 0)
    this.copy(usd = newAmt)
  }

  override def toString: String = {
    val a = map("A")
    val b = map("B")
    val c = map("C")
    val d = map("D")
    s"$id	$usd	$a	$b	$c	$d"
  }
}

object Client {
  def parse(s: String): Client = {
    val arr = s.split("\\s")
    val a = arr(2).toLong
    val b = arr(3).toLong
    val c = arr(4).toLong
    val d = arr(5).toLong
    Client(
      arr(0),
      arr(1).toLong,
      Map("A" -> a,
        "B" -> b,
        "C" -> c,
        "D" -> d
      )
    )
  }
}
