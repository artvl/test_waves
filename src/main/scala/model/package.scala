package object model {

  type Paper = String
  type Amount = Long
  type Price = Long

  sealed trait Direction {
    def opposite: Direction

    def isOpposite(d: Direction): Boolean = opposite == d
  }

  object Direction {
    def apply(s: String): Direction = {
      s match {
        case "s" => SELL
        case "b" => BUY
        case _ => throw new IllegalArgumentException()
      }
    }
  }

  object BUY extends Direction {
    override def opposite: Direction = SELL
  }

  object SELL extends Direction {
    override def opposite: Direction = BUY
  }


  //  object Direction extends Enumeration {
  //    type Direction = Value
  //    val BUY = Value("b")
  //    val SELL = Value("s")
  //
  //    def isOpposite(d1: Direction, d2: Direction): Boolean = {
  //      d1 == BUY && d2 == SELL || d1 == SELL && d2 == BUY
  //    }
  //  }

}
