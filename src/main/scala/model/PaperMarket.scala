package model

import model._

import scala.annotation.tailrec
import scala.collection.immutable.TreeSet

case class PaperMarket(buy: TreeSet[Order], sell: TreeSet[Order]) {
  def placeOrder(order: Order): OpResult = {
    order.direction match {
      case BUY =>
        val (o, newSell, state) = immediateBuy(order, sell, OrderResult.empty())
        if (o.isEmpty()) {
          OpResult(this.copy(sell = newSell), state)
        } else {
          val newBuy = buy + o
          OpResult(this.copy(buy = newBuy, sell = newSell), state)
        }
      case SELL =>
        val (o, newBuy, state) = immediateSell(order, buy, OrderResult.empty())
        if (o.isEmpty()) {
          OpResult(this.copy(buy = newBuy), state)
        } else {
          val newSell = sell + o
          OpResult(this.copy(buy = newBuy, sell = newSell), state)
        }
    }
  }

  @tailrec
  private[model] final def immediateSell(order: Order, treeSet: TreeSet[Order], state: OrderResult): (Order, TreeSet[Order], OrderResult) = {
    if (order.isEmpty() ||
      treeSet.isEmpty ||
      treeSet.last.price < order.price) {
      (order, treeSet, state)
    } else {
      val last = treeSet.last
      if (last.amt > order.amt) {
        val (op, newHead) = last.partiallyComplete(order, last.price)
        val newOrder = order.copy(amt = 0)
        val newSet = (treeSet - last) + newHead
        val newState = state.addOp(op)
        immediateSell(newOrder, newSet, newState)
      } else {
        val (op, newOrder) = order.partiallyComplete(last, last.price)
        val newSet = treeSet - last
        val newState = state.addOp(op)
        immediateSell(newOrder, newSet, newState)
      }
    }
  }

  @tailrec
  private[model] final def immediateBuy(order: Order, treeSet: TreeSet[Order], state: OrderResult): (Order, TreeSet[Order], OrderResult) = {
    if (order.isEmpty() ||
      treeSet.isEmpty ||
      treeSet.head.price > order.price) {
      (order, treeSet, state)
    } else {
      val head = treeSet.head
      if (head.amt > order.amt) {
        val (op, newHead) = head.partiallyComplete(order, head.price)
        val newOrder = order.copy(amt = 0)
        val newSet = (treeSet - head) + newHead
        val newState = state.addOp(op)
        immediateBuy(newOrder, newSet, newState)
      } else {
        val (op, newOrder) = order.partiallyComplete(head, head.price)
        val newSet = treeSet - head
        val newState = state.addOp(op)
        immediateBuy(newOrder, newSet, newState)
      }
    }
  }
}

object PaperMarket {
  def empty(): PaperMarket = new PaperMarket(
    TreeSet.empty[Order](Order.buyOrdering),
    TreeSet.empty[Order](Order.sellOrdering))
}

case class OpResult(market: PaperMarket, orderResult: OrderResult)

case class OrderResult(ops: List[Operation]) {
  def addOp(op: Operation): OrderResult = {
    this.copy(ops = op :: ops)
  }

}

case class Operation(from: String, to: String, paper: Paper, amt: Amount, price: Price) {
  def usdCost: Amount = amt * price
}

object OrderResult {
  def empty(): OrderResult = OrderResult(List.empty)
}