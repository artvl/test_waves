package model

case class Accounts(clients: Map[String, Client]) {

  def updateOrder(order: Order): Order = {
    val c = clients(order.clientId)
    order.direction match {
      case BUY =>
        if (c.enoughUsd(order.amt * order.price)) {
          order
        } else {
          order.copy(amt = 0)
        }
      case SELL =>
        if (c.enoughPaper(order.paper, order.amt)) {
          order
        } else {
          order.copy(amt = 0)
        }
    }
  }

  def bookOrder(order: Order): Accounts = {
    val c = clients(order.clientId)
    val newClient = order.direction match {
      case BUY =>
        c.withdrawUsd(order.amt * order.price)
      case SELL =>
        c.withdrawPaper(order.paper, order.amt)
    }
    val removeC = clients - c.id
    val newClients = removeC + (newClient.id -> newClient)
    new Accounts(newClients)
  }

  def unbookOrder(order: Order): Accounts = {
    val c = clients(order.clientId)
    val newClient = order.direction match {
      case BUY =>
        c.addUsd(order.amt * order.price)
      case SELL =>
        c.addPaper(order.paper, order.amt)
    }
    val removeC = clients - c.id
    val newClients = removeC + (newClient.id -> newClient)
    new Accounts(newClients)
  }

  def applyOperations(ops: List[Operation]): Accounts = {
    ops.foldLeft(this) { (state, op) =>
      state.applyOperation(op)
    }
  }

  def applyOperation(op: Operation): Accounts = {
    if (op.from == op.to) {
      val client = clients(op.to)
      val withoutClient = clients - client.id
      val updatedClient = client.addUsd(op.usdCost).addPaper(op.paper, op.amt)
      Accounts(withoutClient + (updatedClient.id -> updatedClient))
    } else {
      val fromClient = clients(op.from)
      val toClient = clients(op.to)
      val withoutClients = clients - fromClient.id - toClient.id
      val cost = op.price * op.amt
      val updatedFrom = fromClient.addUsd(op.usdCost)
      val updatedTo = toClient.addPaper(op.paper, op.amt)
      val newClients = withoutClients + (updatedTo.id -> updatedTo) + (updatedFrom.id -> updatedFrom)
      Accounts(newClients)
    }
  }

}

object Accounts {
  def create(it: Iterable[Client]): Accounts = {
    val accs = it.map(c => c.id -> c).toMap
    Accounts(accs)
  }
}
