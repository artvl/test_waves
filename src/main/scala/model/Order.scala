package model

import java.util.concurrent.atomic.AtomicInteger


case class Order(clientId: String,
                 direction: Direction,
                 paper: Paper,
                 price: Price,
                 amt: Amount,
                 created: Long) {

  def partiallyComplete(opposite: Order, price: Price): (Operation, Order) = {
    assert(opposite.amt <= amt)
    assert(direction.isOpposite(opposite.direction))
    assert(paper == opposite.paper)
    val order = this.copy(amt = amt - opposite.amt)
    val op = this.direction match {
      case BUY =>
        Operation(
          from = opposite.clientId,
          to = this.clientId,
          paper = this.paper,
          amt = opposite.amt,
          price = price
        )
      case SELL =>
        Operation(
          from = this.clientId,
          to = opposite.clientId,
          paper = this.paper,
          amt = opposite.amt,
          price = price
        )
    }
    (op, order)
  }

  def isEmpty(): Boolean = amt == 0

}

object Order {
  def sellOrdering: Ordering[Order] = Ordering.by { order =>
    (order.price, order.created)
  }

  def buyOrdering: Ordering[Order] = Ordering.by { order =>
    (order.price, -1 * order.created)
  }

  val time = new AtomicInteger(0)

  def parse(s: String): Order = {
    val ts = time.incrementAndGet();
    val arr = s.split("\\s")
    Order(
      clientId = arr(0),
      direction = Direction.apply(arr(1)),
      paper = arr(2),
      price = arr(3).toLong,
      amt = arr(4).toLong,
      created = ts
    )
  }

}
