package model

case class ExchangeState(accountsState: Accounts, map: Map[Paper, PaperMarket]) {


  def updateOrder(order: Order): (Accounts, Order) = {
    val updated = accountsState.updateOrder(order)
    if (updated.amt != 0) {
      val newAcc = accountsState.bookOrder(order)
      (newAcc, order)
    } else {
      (accountsState, order)
    }
  }

  def placeOrder(order: Order): ExchangeState = {
    val (newAccounts, fixedOrder) = updateOrder(order)
    if (order.amt != 0) {
      val coreState = map.getOrElse(fixedOrder.paper, PaperMarket.empty())
      val opRes = coreState.placeOrder(fixedOrder)
      val newCoreState = opRes.market
      val orderRes = opRes.orderResult
      val updatesAccounts = newAccounts.applyOperations(orderRes.ops)
      val mapMinusOld = map - fixedOrder.paper
      val newMap = mapMinusOld + (fixedOrder.paper -> newCoreState)
      ExchangeState(updatesAccounts, newMap)
    } else {
      this
    }
  }

  def cancelAllOrders(): ExchangeState = {
    val allOrders = map.toList.map(_._2).flatMap { market =>
      market.buy.toList ++ market.sell.toList
    }
    val res = allOrders.foldLeft(accountsState) { (acc, order) =>
      acc.unbookOrder(order)
    }
    ExchangeState(res, Map.empty)
  }
}
